#!/usr/bin/env sh

set -e
set -v

terraform init
terraform validate
